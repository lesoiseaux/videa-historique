extends KinematicBody2D

const UP = Vector2(0,-1)
const GRAVITY = 100
const MAX_SPEED = 300
const JUMP = -1500
const ACCELERATION = 50
var velocity = Vector2()
#var can_move
var active_player = global.current_player
var item_count = global.count

func _ready():
	print(str(active_player))

func _physics_process(delta):
	velocity.y += GRAVITY
	var friction = false
	$AnimatedSprite.animation = "marche"
	
	#if can_move:
	if Input.is_action_pressed("ui_right"):
		velocity.x += ACCELERATION
		velocity.x = min(velocity.x+ACCELERATION, MAX_SPEED)
		$AnimatedSprite.animation = "marche"
		if item_count >= 7:
			$AnimatedSprite.animation = "marche_evolution1"
		$AnimatedSprite.flip_h = true
	elif Input.is_action_pressed("ui_left"):
		velocity.x -= ACCELERATION
		velocity.x = max(velocity.x-ACCELERATION, -MAX_SPEED)
		$AnimatedSprite.animation = "marche"
		if item_count >= 7:
			$AnimatedSprite.animation = "marche_evolution1"
		$AnimatedSprite.flip_h = false
	else:
		friction = true
		
		
	if is_on_floor():
		if Input.is_action_just_pressed("ui_up"):
			velocity.y = JUMP
			$AnimatedSprite.animation = "saut"
			if item_count >= 7:
				$AnimatedSprite.animation = "saut_evolution1"
		if friction == true:
			velocity.x = lerp(velocity.x, 0, 0.2)
	else:
		$AnimatedSprite.animation = "saut"
		if item_count >= 7:
			$AnimatedSprite.animation = "saut_evolution1"
		velocity.x = lerp(velocity.x, 0, 0.05)
	
	velocity = move_and_slide(velocity, UP)



