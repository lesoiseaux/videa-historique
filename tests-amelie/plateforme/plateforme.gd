extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
#var playing_now = global.current_player

# Called when the node enters the scene tree for the first time.
func _ready():
	$player1/CameraPlayer1.make_current()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	print(global.count)
	if global.count >= 4 and global.count < 12:
		$player2/AnimatedSprite.animation = "evolution1"
	elif global.count >= 12:
		$player2/AnimatedSprite.animation = "evolution2"
		
	if Input.is_action_just_pressed("ui_down"):
		if $player1/CameraPlayer1.is_current():
			$player1/CameraPlayer1.clear_current()
			$player2/CameraPlayer2.make_current()
			#playing_now = "Player 2"
			#return playing_now
		elif $player2/CameraPlayer2.is_current():
			$player2/CameraPlayer2.clear_current()
			$player1/CameraPlayer1.make_current()
			#playing_now = "Player 1"
			#return playing_now

func _on_Node2D_body_entered(body):
	global.count += 1
	$Node2D.queue_free()
	return global.count

func _on_Node2D2_body_entered(body):
	global.count += 1
	$Node2D2.queue_free()
	return global.count

func _on_Node2D3_body_entered(body):
	global.count += 1
	$Node2D3.queue_free()
	return global.count

func _on_Node2D4_body_entered(body):
	global.count += 1
	$Node2D4.queue_free()
	return global.count
