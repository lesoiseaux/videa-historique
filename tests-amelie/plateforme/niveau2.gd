extends Node2D

var num_frames

func _ready():
	$Camera2D.make_current()
	$RichTextLabel.set_text("Bienvenue dans cet environnement de chance.")
	$femme.play('explosion')
	$femme.set_frame(0)
	
func _process(delta):
	#print(global.count)
	if global.count >= 4 and global.count < 6:
		$player2/AnimatedSprite.animation = "evolution1"
	elif global.count >= 6 and global.count < 9:
		$player1/AnimatedSprite.animation = "marcheevolution1"
		$player2/AnimatedSprite.animation = "evolution1"
	elif global.count >= 9:
		$player2/AnimatedSprite.animation = "evolution2"
		$player1/AnimatedSprite.animation = "sautevolution1"



func _on_Node2D_body_entered(body):
	global.count += 1
	$Node2D.queue_free()
	#$RichTextLabel.clear()
	$RichTextLabel.set_text("Chaque élément est ici choisi au bénéfice des chemins et de la vie.")
	return global.count


func _on_Node2D2_body_entered(body):
	global.count += 1
	$Node2D2.queue_free()
	#$RichTextLabel.clear()
	$RichTextLabel.set_text("Ces particules nourriront vos envies.")
	return global.count


func _on_Node2D3_body_entered(body):
	global.count += 1
	$Node2D3.queue_free()
	#$RichTextLabel.clear()
	$RichTextLabel.set_text("Bienvenue dans cet environnement de chance.")
	return global.count


func _on_Node2D4_body_entered(body):
	global.count += 1
	$Node2D4.queue_free()
	#$RichTextLabel.clear()
	$RichTextLabel.set_text("Une envie partagée se diversifie et amène avec elle ses rêves.")
	return global.count


func _on_Node2D5_body_entered(body):
	global.count += 1
	$Node2D5.queue_free()
	#$RichTextLabel.clear()
	$RichTextLabel.set_text("Le soin de chacune engage notre attention.")
	return global.count


func _on_Node2D6_body_entered(body):
	global.count += 1
	$Node2D6.queue_free()
	#$RichTextLabel.clear()
	$RichTextLabel.set_text("Associons les images et les amours.")
	return global.count


func _on_Node2D7_body_entered(body):
	global.count += 1
	$Node2D7.queue_free()
	#$RichTextLabel.clear()
	$RichTextLabel.set_text("With those we love alive.")
	return global.count


func _on_Node2D8_body_entered(body):
	global.count += 1
	$Node2D8.queue_free()
	#$RichTextLabel.clear()
	$RichTextLabel.set_text("La capacité d'agir réside en notre échange.")
	return global.count


func _on_Node2D9_body_entered(body):
	global.count += 1
	$Node2D9.queue_free()
	#$RichTextLabel.clear()
	$RichTextLabel.set_text("Feront croître chacune de vos histoires.")
	return global.count


func _on_Node2D10_body_entered(body):
	global.count += 1
	$Node2D10.queue_free()
	return global.count


func _on_femme_animation_finished():
	print('finished')
	$femme.queue_free()
