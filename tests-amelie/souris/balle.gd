extends RigidBody2D

export var min_speed = 100
export var max_speed = 230

func _ready():
	pass

func _on_bulle_body_entered(body):
	queue_free()

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
