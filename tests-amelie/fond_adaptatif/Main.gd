extends Node2D

var obstacle = preload("res://obstacle.tscn")
var newSprite = preload("res://sprite_fond.tscn")
var new_obstacle = obstacle.instance()
var replace
var screen_size
var camera1
var camera2

func _ready():
	var timer = Timer.new()
	timer.set_one_shot(true)
	timer.set_wait_time(5)
	timer.connect("timeout", self, "_on_Timer_timeout")
	add_child(timer)
	timer.start()
	screen_size = get_viewport_rect().size
	$Personnage2/GUI.visible = true
	$Personnage1/GUI.visible = true
	$Personnage1/CameraPlayer1.make_current()
	camera1 = $Personnage1/CameraPlayer1
	camera2 = $Personnage2/CameraPlayer2
	

func _on_Timer_timeout():
	print("Time out")
	replace = true
	return replace

func _process(delta):
	if camera1.is_current():
		$Personnage1/GUI.visible = true
		$Personnage2/GUI.visible = false
		global.current_character_is_1 = true 
		if Input.is_action_just_pressed("ui_select"):
			$Personnage1/CameraPlayer1.clear_current()
			$Personnage2/CameraPlayer2.make_current()
		return global.current_character_is_1
			
	else:
		$Personnage2/GUI.visible = true
		$Personnage1/GUI.visible = false
		global.current_character_is_1 = false
		if Input.is_action_just_pressed("ui_select"):
			$Personnage2/CameraPlayer2.clear_current()
			$Personnage1/CameraPlayer1.make_current()
		return global.current_character_is_1


func _on_carreFond_body_entered(body):
	if replace != true:
		print("collision")
		$Sprite.visible = false
