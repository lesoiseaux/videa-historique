extends MarginContainer

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if global.current_character_is_1:
		$Background/Number.set_text(str(global.items_p1))
	else:
		$Background/Number.set_text(str(global.items_p2))