extends Node

export var num_hills = 2
export var slice = 10
export var hill_range = 40

var screensize
var terrain = Array()
var texture = preload("res://green.png")

# getting the size of the viewport and starting the generative process in its middle
func _ready():
	randomize()
	screensize = get_viewport().get_visible_rect().size
	terrain = Array()
# positioning the starting point of the hill on the y axis in relation to the viewport
	var start_y = screensize.y * 2/3 + (-hill_range + randi() % hill_range*2)
#keeps the memory of the last hill_point
	terrain.append(Vector2(0 - screensize.x/2, start_y))
	add_hills()
	
func _process(delta):
#Add hills if the memorized value is inferior by more then half to the character position in the screen
	if terrain[-1].x < get_node("../ane").position.x + screensize.x / 2:
		add_hills()

# func _input(event):
# checking wether the mouse is clicked
#	if event is InputEventMouseButton:
#		get_node("../kneel").position = event.position
#		print(event.position)

func add_hills():
#Calculating the number of hills in one screen
	var hill_width = screensize.x / num_hills
#Calculating the amplitude of the hills
	var hill_slices = hill_width / slice
#matrix that keeps one element
	var start = terrain[-1]
#polygone filling the screen 
	var poly = PoolVector2Array()

#for each hills apply the following
	for i in range(num_hills):
#randomize the width of the hills
		var height = randi() % hill_range
#define the starting point on y axis
		start.y -= height
#for each hill_slice apply the following
		for j in range(0, hill_slices):
#define the curve (max and min point)
			var hill_point = Vector2()
			hill_point.x = start.x + j * slice + hill_width * i
			hill_point.y = start.y + height * cos(2 * PI / hill_slices * j)
			#$Line2D.add_point(hill_point)
#append terrain and polygon with the new information: the new hill_point
			terrain.append(hill_point)
			poly.append(hill_point)
		start.y += height
#define the shape that will allow to colide with the hills
	var shape = CollisionPolygon2D.new()
#define the bottom limit of that shape
	var ground = Polygon2D.new()
	$StaticBody2D.add_child(shape)
#append the coordinates to this new shape
	poly.append(Vector2(terrain[-1].x, screensize.y))
	poly.append(Vector2(start.x, screensize.y))
	shape.polygon = poly
	ground.polygon = poly
	ground.texture = texture
	add_child(ground)
		
		
