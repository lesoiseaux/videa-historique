%!PS-Adobe-3.0 EPSF-3.0
%%BoundingBox: -13 -4 47 90 
%%HiResBoundingBox: -12.50403 -4 46.51968 89.03935 
%%Creator: MetaPost 2.000
%%CreationDate: 2021.10.26:1906
%%Pages: 1
%%DocumentResources: procset mpost-minimal
%%DocumentSuppliedResources: procset mpost-minimal
%%EndComments
%%BeginProlog
%%BeginResource: procset mpost-minimal
/bd{bind def}bind def/fshow {exch findfont exch scalefont setfont show}bd
/fcp{findfont dup length dict begin{1 index/FID ne{def}{pop pop}ifelse}forall}bd
/fmc{FontMatrix dup length array copy dup dup}bd/fmd{/FontMatrix exch def}bd
/Amul{4 -1 roll exch mul 1000 div}bd/ExtendFont{fmc 0 get Amul 0 exch put fmd}bd
/ScaleFont{dup fmc 0 get Amul 0 exch put dup dup 3 get Amul 3 exch put fmd}bd
/SlantFont{fmc 2 get dup 0 eq{pop 1}if Amul FontMatrix 0 get mul 2 exch put fmd}bd
%%EndResource
%%EndProlog
%%BeginSetup
%%EndSetup
%%Page: 1 1
 0 0 0 setrgbcolor 0 8 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 5.6692 62.3621 moveto
17.15556 42.46709 14.88564 17.49759 0 0 curveto stroke
newpath 42.51968 85.03935 moveto
42.51968 66.1127 22.56511 53.83295 5.6692 62.3621 curveto stroke
newpath -8.50403 35.43306 moveto
3.68544 37.58237 16.15698 37.58237 28.34645 35.43306 curveto stroke
showpage
%%EOF
