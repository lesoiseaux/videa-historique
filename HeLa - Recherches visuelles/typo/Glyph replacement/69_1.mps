%!PS-Adobe-3.0 EPSF-3.0
%%BoundingBox: -18 -1 45 82 
%%HiResBoundingBox: -17.25581 -0.2482 44.18486 81.03532 
%%Creator: MetaPost 2.000
%%CreationDate: 2021.05.28:1811
%%Pages: 1
%%DocumentResources: procset mpost-minimal
%%DocumentSuppliedResources: procset mpost-minimal
%%EndComments
%%BeginProlog
%%BeginResource: procset mpost-minimal
/bd{bind def}bind def/fshow {exch findfont exch scalefont setfont show}bd
/fcp{findfont dup length dict begin{1 index/FID ne{def}{pop pop}ifelse}forall}bd
/fmc{FontMatrix dup length array copy dup dup}bd/fmd{/FontMatrix exch def}bd
/Amul{4 -1 roll exch mul 1000 div}bd/ExtendFont{fmc 0 get Amul 0 exch put fmd}bd
/ScaleFont{dup fmc 0 get Amul 0 exch put dup dup 3 get Amul 3 exch put fmd}bd
/SlantFont{fmc 2 get dup 0 eq{pop 1}if Amul FontMatrix 0 get mul 2 exch put fmd}bd
%%EndResource
%%EndProlog
%%BeginSetup
%%EndSetup
%%Page: 1 1
 0 0 0 setrgbcolor 9 0 dtransform exch truncate exch idtransform pop setlinewidth
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 0 4.2518 moveto
0 76.53532 lineto stroke
 0 9 dtransform truncate idtransform setlinewidth pop
newpath -12.75581 43.93709 moveto
36.85048 76.53532 lineto stroke
newpath 25.51163 36.85048 moveto
0 36.85048 lineto stroke
newpath 39.68486 4.2518 moveto
0 4.2518 lineto stroke
showpage
%%EOF
